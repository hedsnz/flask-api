# How to develop and test a minimal API in Python with Flask

This project is a minimal (albeit tested and documented) API, written
in Python using Flask. Inspiration for this project comes from a
<a href="https://github.com/udacity/cd0037-API-Development-and-Documentation-exercises">Udacity course</a>
on the same subject. A lot of the code in this repo is very closely
(and in some cases, verbatim) copied from the Udacity course.
The key difference is that this project is more minimal,
with fewer dependencies: There is no React front-end, and we use SQLite instead of PostgreSQL/SQLAlchemy for the database.

## Getting started

Create the project folder and virtual environment, install dependencies:

```
mkdir flask-api
cd flask-api
python3 -m venv .env
source .env/bin/activate
pip install -r requirements.txt
```

### Database setup

Run the following in the root flask-api directory to initialise the production and test databases:

```
flask --app flaskr init-test-db
flask --app flaskr init-prod-db
```

### Running tests

```
python -m unittest discover
```

### Running the app

```
flask --app flaskr run --debug
```

## API reference

### Getting started

- Base URL: This app hasn't been deployed, so only runs
locally at the default URL, `http://127.0.0.1:5000/`.
- Authentication: There is no authentication and no API keys required.

### Error handling

Errors are returned as JSON objects in the following format:

```
{
    "success": False,
    "error": <HTTP error code>,
    "message": <HTTP error message>,
}
```

For example, an HTTP 400 error returns the following:

```
{
    "success": False,
    "error": 400,
    "message": "Bad request",
}
```

Error types returned by the API include the following:

- 400 Bad request
- 404 Resource not found
- 422 Unprocessable content
- 500 Internal server error

### Endpoints

#### GET /questions

Returns a set of questions, paginated in groups of five.

Request parameters:

- `page`: Integer specifying which page of (five) questions
to return.
- `category`: Character string of an existing question category.
- `search`: Character string to execute a partial string match
on question text.

JSON response body:

- `number_of_questions`: Integer, total number of questions.
- `success`: True (on successful request).
- `questions`: List of questions (question text only).
- `current_category`: Selected category (or null if not provided).
- `categories`: List of all categories.

Examples:

```
# Return the first five questions
curl 127.0.0.1:5000/questions

# Return the second page of questions (questions 6 to 10)
curl 127.0.0.1:5000/questions?page=2

# Return (up to) the first five questions in the History category
curl 127.0.0.1:5000/questions?category=art

# Return (up to) the first five questions containing a partial
string match for the search term "title"
curl 127.0.0.1:5000/questions?search=title

# Return (up to) the first five questions containing a partial
# string match for the search term "title", within the History category
curl 127.0.0.1:5000/questions?category=history&search=title
```
curl 127.0.0.1:5000/questions/1

#### GET /questions/<id>

Returns a specific question by ID. 

Request parameters:

- `id`: Integer value corresponding to an ID of a
question in the Question table.

JSON response body:

- `success`: True (on successful request).
- `question`: Dictionary containing five keys: `question` (question text), `answer` (answer text), `id` (question ID), `difficulty` (integer difficulty from 1 to 5), `category_id` (foreign key to a specific entry in the Category database table).

Example:

```
# Return full detail for question with ID == 1
curl 127.0.0.1:5000/questions/1
```

#### DELETE /questions/<id>

Deletes a specific question by ID.

Request parameters:

- `id`: Integer value corresponding to an ID of a
question in the Question table.

JSON response body:

- `success`: True (on successful request).
- `deleted`: ID of deleted question.

Example:

```
curl -X DELETE 127.0.0.1:5000/questions/1
```

#### POST /questions

Create a new question using a provided question text, answer text,
category ID, and difficulty rating.

Request parameters:

- `question`: question text.
- `answer`: answer text.
- `category_id`: ID of record in Category table.
- `difficulty`: integer difficulty from 1 to 5.

JSON response body:

- `success`: True (on successful request).
- `created`: ID of created question.

Example:

```
curl -X POST -H "Content-Type: application/json" --data '{"question": "Does this work?", "answer": "Hopefully, yes", "category_id": 1, "difficulty": 5}' 127.0.0.1:5000/questions
```

#### GET /categories

Get all categories. 

No request parameters.

JSON response body:

- `number_of_categories`: Integer, total number of categories.
- `success`: True (on successful request).
- `categories`: List of category objects. The category objects are a dictionary containing two keys: `id` (category ID), and
`type` (category text).

Example:

```
curl 127.0.0.1:5000/categories
```

#### GET /categories/<id>/questions

Get all questions in a given category.

Request parameters:

- `id`: Integer corresponding to the ID of a category.

JSON response body:

- `success`: True (on successful request).
- `current_category`: String, category type corresponding to the
ID supplies in the request.
- `questions`: List of question dictionary objects.

Example:

```
curl 127.0.0.1:5000/categories/1/questions
```

#### POST /quizzes

Play the quiz.

Request parameters:

- `previous_questions`: Array of integers corresponding to
question IDs of previously-asked questions.
- `quiz_category`: (Optional) Character string corresponding to the
current category of the quiz.

JSON response body:

- `success`: True (on successful request).
- `question`: A single new question dictionary, containing:
    - `id`: ID of question.
    - `question`: Question text.
    - `answer`: Answer text.
    - `difficulty`: Difficulty rating (integer from 1 to 5).
    - `category`: Category text.

If there are no questions within the given category that
haven't been previously asked/answered, then the `question`
object is an empty dictionary.

Example:

```
# No previous questions, any category
curl -X POST -H "Content-Type: application/json" --data '{"previous_questions": []}' 127.0.0.1:5000/quizzes

# Single previous question, any category
curl -X POST -H "Content-Type: application/json" --data '{"previous_questions": [1]}' 127.0.0.1:5000/quizzes

# Multiple previous questions, any category
curl -X POST -H "Content-Type: application/json" --data '{"previous_questions": [1, 2, 3, 4, 5, 6]}' 127.0.0.1:5000/quizzes

# No previous questions, art category
curl -X POST -H "Content-Type: application/json" --data '{"previous_questions": [], "quiz_category": "art"}' 127.0.0.1:5000/quizzes

# Single previous question, art category
curl -X POST -H "Content-Type: application/json" --data '{"previous_questions": [4], "quiz_category": "art"}' 127.0.0.1:5000/quizzes

# Multiple previous questions, art category
curl -X POST -H "Content-Type: application/json" --data '{"previous_questions": [4, 5, 6], "quiz_category": "art"}' 127.0.0.1:5000/quizzes

# No unasked questions in category left!
curl -X POST -H "Content-Type: application/json" --data '{"previous_questions": [4, 5, 6, 7], "quiz_category": "art"}' 127.0.0.1:5000/quizzes
```